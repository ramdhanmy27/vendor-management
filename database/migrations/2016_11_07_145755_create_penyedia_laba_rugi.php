<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaLabaRugi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_laba_rugi", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->text("deskripsi");
            $table->date("posisi");
            $table->char("mata_uang", 1);
            $table->decimal("penjualan", 15, 2);
            $table->decimal("harga_pokok", 15, 2);
            $table->decimal("laba", 15, 2);
            $table->decimal("rugi", 15, 2);
            $table->decimal("biaya", 15, 2);
            $table->decimal("laba_bersih_sebp", 15, 2);
            $table->decimal("pajak", 15, 2);
            $table->decimal("laba_bersih_setp", 15, 2);
            $table->boolean("att");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_laba_rugi");
    }
}
