<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisPenyedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_penyedia', function(Blueprint $table) {
            $table->string('kode', 5)->primary();
            $table->string('nama', 20);
        });
        
        Schema::create('detail_jenis_penyedia', function(Blueprint $table) {
            $table->string('kode', 5)->primary();
            $table->string('kode_jenis_penyedia', 5);
            $table->string('nama', 50);
        });

        $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_jenis_penyedia');
        Schema::drop('jenis_penyedia');
    }

    public function seed() {
        $jenis = [
            "BU" => "Badan Usaha",
            "BH" => "Badan Hukum",
            "OR" => "Perorangan",
        ];

        $detail = [
            "BU" => [
                "PT" => "Perseroan Terbatas",
                "KP" => "Koperasi",
                "CV" => "Persekutuan Komanditer",
                "UD" => "Usaha Dagang",
            ],
            "BH" => [
                "LR" => "Lembaga Riset",
                "IP" => "Instansi Pemerintah",
                "LS" => "LSM Nasional",
                "KM" => "Kelompok Masyarakat",
                "PG" => "Perguruan Tinggi",
            ],
        ];

        foreach ($jenis as $kode => $nama) {
            if (!isset($detail[$kode]))
                continue;
            
            foreach ($detail[$kode] as $kode_det => $nama) {
                $detail[$kode][$kode_det] = [
                    "kode" => $kode_det,
                    "kode_jenis_penyedia" => $kode,
                    "nama" => $nama,
                ];
            }

            DB::table("jenis_penyedia")->insert(["kode" => $kode, "nama" => $nama]);
            DB::table("detail_jenis_penyedia")->insert($detail[$kode]);
        }
    }
}
