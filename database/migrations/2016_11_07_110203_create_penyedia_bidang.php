<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaBidang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_bidang", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->string("kode_bidang", 6);
            $table->string("kode_sub_bidang", 6);
            $table->string("no_kontrak", 20);
            $table->string("keterangan", 100);
            $table->boolean('att');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_bidang");
    }
}
