<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyedia', function(Blueprint $table) {
            $table->string('kode', 10)->primary();
            $table->string('kode_jenis_penyedia', 5);
            $table->string('email')->unique();
            $table->string('email_instansi')->unique()->nullable();
            $table->char('kode_pos', 6)->nullable();
            $table->char('kode_provinsi', 2)->nullable();
            $table->char('kode_kota', 4)->nullable();
            $table->char('status', 1);
            $table->char('raport', 1);
            $table->timestamps();
            $table->dateTime("approved_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penyedia');
    }
}
