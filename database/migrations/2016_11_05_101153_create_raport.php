<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRaport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raport', function(Blueprint $table) {
            $table->char('id', 1)->primary();
            $table->string('nama', 10);
            $table->char('color', 7);
        });

        $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('raport');
    }

    public function seed()
    {
        $raport = [
            ["id" => 1, "nama" => "Hijau", "color" => "#44aa33"],
            ["id" => 2, "nama" => "Kuning", "color" => "#ffcc00"],
            ["id" => 3, "nama" => "Merah", "color" => "#990000"],
            ["id" => 4, "nama" => "Hitam", "color" => "#333333"],
        ];

        DB::table("raport")->insert($raport);
    }
}
