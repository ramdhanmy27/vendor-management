<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubBidang extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("sub_bidang", function(Blueprint $table) {
			// $table->increments("id");
			$table->string("kode_bidang", 10);
			$table->string("kode", 10)->primary();
			$table->string("nama");
			$table->string("keterangan");
			$table->boolean("status");
		});

		$this->seed();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("sub_bidang");
	}

	public function seed() 
	{
		$ref = [
			["A.01.00", "A.01.01", "Peralatan/suku cadang pemboran, eksplorasi dan produksi", "Drilling exploration and production; equipment, parts and accessories"],
			["A.01.00", "A.01.02", "Selubung sumur, pipa produksi dan kelengkapannya", "Well casing, tubing and accessories"],
			["A.01.00", "A.01.03", "Peralatan/bahan lumpur/kimia pemboran dan penyemenan", "Drilling mud and cementing; equipment and materials"],
			["A.01.00", "A.01.04", "Peralatan/suku cadang boiler, mesin, turbin, pembangkit listrik, pompa dan kompresor", "Boilers, engines, turbines, generators, pumps and compressors; equipment, accessories and parts"],
			["A.01.00", "A.01.05", "Peralatan/suku cadang pengolah dan pemurni minyak/gas /kimia.", "Oil, gas and chemical; plants, accessories and parts."],
			["A.02.00", "A.02.01", "Peralatan/suku cadang pengemas, pengangkat dan pengangkut", "Container, packing, hoisting /lifting and transportation; equipment and parts"],
			["A.02.00", "A.02.02", "Peralatan/suku cadang bangunan, jalan dan konstruksi", "Building, road and construction; equipments and parts"],
			["A.02.00", "A.02.03", "Peralatan/suku cadang instrumentasi dan kelengkapan mesin", "Instrumentation and machinery accessories."],
			["A.02.00", "A.02.04", "Peralatan/suku cadang mekanikal serta elektrikal", "Mechanical and electrical; equipment and parts"],
			["A.02.00", "A.02.05", "Pipa, selang, katup, dan penyambung", "Tubular goods, hoses, valve, fitting and flanges"],
			["A.02.00", "A.02.06", "Peralatan/suku cadang telekomunikasi, navigasi, dan komputer", "Telecommunication, navigation and computers; equipment, accessories and parts"],
			["A.02.00", "A.02.07", "Peralatan/suku cadang alat ukur, survai dan laboratorium", "Measuring, survey and laboratory; equipment and accessories"],
			["A.02.00", "A.02.08", "Alat-alat kerja dan peralatan bengkel", "Tools and other shop equipmen"],
			["A.02.00", "A.02.09", "Peralatan/suku cadang keselamatan kerja, pemadam kebakaran dan lindungan lingkungan", "Fire safety and environmental conservation equipments and materials"],
			["A.02.00", "A.02.10", "Peralatan/bahan bangunan/ tangki, bahan metal/ bukan metal, tali baja, rantai, bahan kemasan, bahan pengikat dan kelengkapannya.", "Buildings and tanks materials, metals, non metals, ropes, chains, packaging, fasteners and general hardware."],
			["A.03.00", "A.03.01", "Bahan kimia, bahan bakar, pelumas dan cat", "Chemicals, fuel, lubricant and paints"],
			["A.03.00", "A.03.02", "Peralatan/suku cadang/bahan peledak, senjata api dan amunisi", "Explosives, gun and ammunitions"],
			["A.04.00", "A.04.01", "Peralatan/perlengkapan tulis, barang cetakan, kantor, pendidikan, peragaan/ visualisasi, olah raga, kesenian, pergudangan dan perlengkapan pegawai", "Ofice, warehouse and personnel; equipment, accessories and materials"],
			["A.04.00", "A.04.02", "Peralatan/suku cadang/bahan pertanian, perkebunan, peternakan, perikanan, dan kehutanan", "Agricultural equipment and parts"],
			["A.04.00", "A.04.03", "Peralatan/suku cadang kesehatan, farmasi dan obat-obatan", "Medical equipment, accessories and supplies"],
			["A.04.00", "A.04.04", "Peralatan, perlengkapan, perabotan dan bahan-bahan kebutuhan rumah tangga", "Furniture, household and housing/club requisites"],
			["B.01.00", "B.01.01", "Drainase dan jaringan pengairan", "Drainage, channel and other water system"],
			["B.01.00", "B.01.02", "Jalan, jembatan, landasan dan lokasi pemboran darat/rawa", "Roads, bridges, field and onshore/swamp drilling foundation"],
			["B.01.00", "B.01.03", "Fasilitas produksi darat", "Onshore production facilities"],
			["B.01.00", "B.01.04", "Fasilitas produksi dan platform lepas pantai", "Ofshore production platform and facilities"],
			["B.01.00", "B.01.05", "Jalur pipa dan fasilitas pendukungnya", "Pipeline and supports"],
			["B.01.00", "B.01.06", "Gedung dan pabrik", "Building and plants"],
			["B.01.00", "B.01.07", "Bangunan pengolahan air bersih dan limbah", "Water and waste treatment"],
			["B.01.00", "B.01.08", "Reklamasi dan pengerukan", "Reclamation and dredging"],
			["B.01.00", "B.01.09", "Dermaga, penahan gelombang dan penahan tanah (talud)", "Jetty, pier and bandwall"],
			["B.01.00", "B.01.10", "Pemboran air tanah", "Water well drilling"],
			["B.01.00", "B.01.11", "Pertamanan dan lanskap", "Gardening and landscaping"],
			["B.01.00", "B.01.12", "Perumahan dan permukiman", "Housing"],
			["B.01.00", "B.01.13", "Bendung dan bendungan", "Dam"],
			["B.01.00", "B.01.14", "Interior", "Interior."],
			["B.02.00", "B.02.01", "Pembangkitan dan jaringan transmisi listrik", "Generating and electric transmission"],
			["B.02.00", "B.02.02", "Instalasi kelistrikan dan jasa kelistrikan lain", "Electric installation and other electrical services"],
			["B.02.00", "B.02.03", "Tata Udara", "Air conditioning"],
			["B.02.00", "B.02.04", "Pekerjaan mekanikal", "Mechanical services"],
			["B.02.00", "B.02.05", "Pabrikasi platform, SBM, DBM, struktur dan pancang", "Platform, mooring facility, structure and pile"],
			["B.02.00", "B.02.06", "Pabrikasi vessels, heat exchanger, heater, bolier, tanki dan pipa", "Vessels, heat exchanger, heater, boiler, tanks and pipe fabrication"],
			["B.02.00", "B.02.07", "Pemasangan alat angkut dan alat angkat", "Hoist, lift and convey facilities"],
			["B.02.00", "B.02.08", "Pemasangan fasilitas produksi dan fasilitas lain lepas pantai", "Ofshore production facilities."],
			["B.03.00", "B.03.01", "Meteorologi dan geofisika", "Meteorology &geophysics"],
			["B.03.00", "B.03.02", "Radio, telekomunikasi, sarana bantu navigasi laut/udara, rambu sungai dan peralatan SAR", "Radio, telecommunication, navigation and SAR facilities"],
			["B.03.00", "B.03.03", "Pemasangan jaringan teknologi informasi dan telekomunikasi", "Information and communication technology network installation"],
			["B.03.00", "B.03.04", "Pemasangan & pemeliharaan instrumentasi", "Instrument installation & maintenance."],
			["B.04.00", "B.04.01", "Pembangunan dan reparasi kapal", "Ship building and repair"],
			["B.04.00", "B.04.02", "Pengangkatan kerangka kapal", "Vessel floatation"],
			["B.04.00", "B.04.03", "Pembesituaan kapal dan fasilitas produksi", "Marine vessels and production facilities scraping"],
			["B.04.00", "B.04.04", "Barak, peti kemas dan Iain-Iain yang sejenis", "Living quarter/cabins/ camps and container"],
			["B.04.00", "B.04.05", "Pengecoran dan pembentukan", "Forging and forming."],
			["B.05.00", "B.05.01", "Pembibitan/pembenihan tanaman dan perikanan", "Agriculture and fish nursery"],
			["B.05.00", "B.05.02", "Reboisasi/penghijauan", "Replanting and plantation."],
			["B.06.00", "B.06.01", "Penjajagan dan pemindaian", "Survey and seismic"],
			["B.06.00", "B.06.02", "Pemboran darat", "Onshore drilling services"],
			["B.06.00", "B.06.03", "Pemboran rawa dan lepas pantai", "Ofshore drilling services"],
			["B.06.00", "B.06.04", "Pemboran terarah", "Directional drilling services"],
			["B.06.00", "B.06.05", "Pemboran inti", "Coring services"],
			["B.06.00", "B.06.06", "Pemboran seismic", "Seismic drilling services"],
			["B.06.00", "B.06.07", "Pemboran hidrolik", "Hydraulic drilling services"],
			["B.06.00", "B.06.08", "Pekerjaan pemancingan", "Fishing services"],
			["B.06.00", "B.06.09", "Rekayasa lumpur dan jasa lumpur lainnya", "Mud logging, mud engineering and mud services"],
			["B.06.00", "B.06.10", "Loging dan perforasi", "Well logging and perforating services"],
			["B.06.00", "B.06.11", "Penyemenan sumur", "Well cementing services"],
			["B.06.00", "B.06.12", "Pengujian lapisan bawah tanah", "DST test"],
			["B.06.00", "B.06.13", "Pengujian produksi sumur", "Well production test"],
			["B.06.00", "B.06.14", "Pemasangan dan perawatan pom pa produksi", "Oil well pump installation and maintenance services"],
			["B.06.00", "B.06.15", "Stimulasi sumur dan penambangan sekunder", "Well stimulation and secondary recovery services"],
			["B.06.00", "B.06.16", "Pekerjaan ulang/work over dan perawatan sumur", "Work over and well maintenance services (including wireline and snubbing)"],
			["B.06.00", "B.06.17", "Pelayanan casing dan tubing", "Casing and tubing setting services"],
			["B.06.00", "B.06.18", "Perawatan fasilitas produksi", "Production facilities maintenance services"],
			["B.06.00", "B.06.19", "Penyewaan menara pemboran darat atau laut", "Onshore and offshore drilling rig provision"],
			["B.06.00", "B.06.20", "Penyewaan alat penyimpanan minyak dan gas, di darat atau laut", "Onshore & offshore oil and gas storage facilities provison"],
			["B.06.00", "B.06.21", "Penyewaan jaringan pipa pengaliran minyak dan gas bumi", "Provision: oil and gas trunk line"],
			["B.06.00", "B.06.22", "Jasa pertambangan minyak dan gas bumi lainnya", "Miscellaneous oil gas mining services."],
			["C.00.00", "C.00.01", "Percetakan dan penjilidan", "Printing and binding", ""],
			["C.00.00", "C.00.02", "Pemeliharaan/perbaikan alat/peralatan kantor", "Maintenance and repair: ofice equipment"],
			["C.00.00", "C.00.03", "Pemeliharaan/perbaikan alat/peralatan angkutan darat/laut/udara", "Maintenance and repair: land/sea/air transport equipment"],
			["C.00.00", "C.00.04", "Pemeliharaan/perbaikan pustaka, barang-barang awetan, fauna dan Iain-Iain", "Maintenance and repair: library, preserved items, fauna etc"],
			["C.00.00", "C.00.05", "Jasa pembersihan, pest control, termite control dan fumigasi", "Cleaning, pest control, termite control and fumigation"],
			["C.00.00", "C.00.06", "Pengepakan, pengangkutan, pengurusan dan penyampaian barang melalui darat/laut/udara", "Packing, transportation and courier services"],
			["C.00.00", "C.00.07", "Penjah itan/kon peksi", "Convection"],
			["C.00.00", "C.00.08", "Jasa boga", "Catering"],
			["C.00.00", "C.00.09", "Jasa importir/eksportir", "Importation /exportation formalities"],
			["C.00.00", "C.00.10", "Perawatan komputer, alat/peralatan elektronik/telekomunikasi", "Maintenance and repair: computers, electronics /telecommunication equipments"],
			["C.00.00", "C.00.11", "Iklan/reklame, film, pemotretan", "Advertising, film and photography"],
			["C.00.00", "C.00.12", "Jasa penulisan dan penerjemahan", "Writing and translation"],
			["C.00.00", "C.00.13", "Penyedia tenaga kerja", "Labor supply"],
			["C.00.00", "C.00.14", "Penyewaan alat angkutan darat/laut/udara", "Rental/lease: transport equipment"],
			["C.00.00", "C.00.15", "Penyewaan peralatan kerja/produksi/konstruksi", "Rental/lease: tools, production/ construction equipment"],
			["C.00.00", "C.00.16", "Penyewaan peralatan/ perlengkapan pemboran", "Rental: Drilling tools"],
			["C.00.00", "C.00.17", "Penyewaan rumah, kantor, lapangan penumpukan, gudang dan perlengkapan terkait", "Lease: Housing, offices, yards, landing yards, warehouse and related equipment"],
			["C.00.00", "C.00.18", "Jasa penyelaman/pekerjaan bawah air", "Diving and underwater services"],
			["C.00.00", "C.00.19", "Jasa asuransi", "Insurance"],
			["C.00.00", "C.00.20", "Pengadaan/pembebasan tanah", "Land procurement and formalities"],
			["C.00.00", "C.00.21", "Akomodasi dan angkutan penumpang", "Accommodation and travel"],
			["C.00.00", "C.00.22", "Pekerjaan jasa Iain-Iain", "Miscellaneous services."],
			["D.01.00", "D.01.01", "Bangunan gedung dan bangunan pabrik", "Building and plants"],
			["D.01.00", "D.01.02", "Teknik lingkungan", "Environment technology"],
			["D.01.00", "D.01.03", "Jalan dan jembatan", "Road and bridges"],
			["D.01.00", "D.01.04", "Jaringan", "Transmission network"],
			["D.01.00", "D.01.05", "Bendung dan waduk", "Water dam"],
			["D.01.00", "D.01.06", "Sungai, rawa dan pantai", "River, swamp and shore"],
			["D.01.00", "D.01.07", "Perumahan dan pemukiman", "Housing and camps"],
			["D.01.00", "D.01.08", "Technologi kelautan", "Sea technology"],
			["D.02.00", "D.02.01", "Transportasi darat, laut, udara, sungai dan penyeberangan", "Land, sea, air, river and channel transportation"],
			["D.02.00", "D.02.02", "Sarana/prasarana transportasi darat", "Transport equipment and infrastructures: land"],
			["D.02.00", "D.02.03", "Sarana/prasarana transportasi laut", "Sea transport equipment and infrastructures"],
			["D.02.00", "D.02.04", "Sarana/prasarana transportasi udara", "Air transport equipment and infrastructures"],
			["D.02.00", "D.02.05", "Sarana/prasarana transportasi sungai dan penyeberangan", "Transport equipment and infrastructures: river and channel"],
			["D.02.00", "D.02.06", "Sistem terminal", "Terminal system"],
			["D.02.00", "D.02.07", "Angkutan barang", "Goods transportation"],
			["D.03.00", "D.03.01", "Sistem/teknologi pos dan telekomunikasi", "Post and telecommunication system"],
			["D.03.00", "D.03.02", "Pariwisata dan perhotelan", "Tourism and hotels."],
			["D.04.00", "D.04.01", "Perkebunan/pertanian/ peternakan", "Farming"],
			["D.04.00", "D.04.02", "Kehutanan", "Forestry"],
			["D.04.00", "D.04.03", "Perikanan", "Fishery"],
			["D.04.00", "D.04.04", "Konservasi dan Penghijauan", "Conservation and replant"],
			["D.04.00", "D.04.05", "Pertanian lainnya", "Other agriculture services."],
			["D.05.00", "D.05.01", "Industri Mesin dan Logam", "Machinery and metals"],
			["D.05.00", "D.05.02", "Industri Kimia", "Chemistry"],
			["D.05.00", "D.05.03", "Industri Hasil Pertanian", "Agriculture"],
			["D.05.00", "D.05.04", "Industri Elektronika", "Electronics"],
			["D.05.00", "D.05.05", "Industri Bahan Bangunan", "Building materials"],
			["D.05.00", "D.05.06", "Perindustrian lainnya", "Other industrial services."],
			["D.06.00", "D.06.01", "Distribusi dan Transmisi", "Electric transmission and distribution"],
			["D.06.00", "D.06.02", "Pembangkitan tenaga lainnya", "Other power generating services."],
			["D.07.00", "D.07.01", "Jasa survey", "Survey"],
			["D.07.00", "D.07.02", "Perencanaan Umum", "General planning"],
			["D.07.00", "D.07.03", "Study kelayakan", "Feasibility study"],
			["D.07.00", "D.07.04", "Perencanaan teknis", "Technical engineering and planning"],
			["D.07.00", "D.07.05", "Penelitian", "Research and study"],
			["D.07.00", "D.07.06", "Pengawasan", "Supervision"],
			["D.07.00", "D.07.07", "Manajemen.", "Management"],
			["D.08.00", "D.08.01", "Asuransi, Perbankan, Keuangan", "Insurance, banking and finance"],
			["D.08.00", "D.08.02", "Kesehatan, Pendidikan, Sumber Daya Manusia, Kependudukan", "Health, education, human resources and community"],
			["D.08.00", "D.08.03", "Hukum dan penerangan", "Legal and information"],
			["D.08.00", "D.08.04", "Sub bidang lainnya", "Other consultancy services"],
		];

		$data = [];
		foreach ($ref as $item) {
			$data[] = [
				"kode_bidang" => current($item),
				"kode" => next($item),
				"nama" => next($item),
				"keterangan" => next($item),
				"status" => true,
			];
		}

		DB::table("sub_bidang")->insert($data);
	}
}
