<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaIjinUsaha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_ijin_usaha", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->integer("id_jenis_ijin");
            $table->string("no_surat", 20);
            $table->string("instansi", 100);
            $table->date("berlaku_start");
            $table->date("berlaku_end");
            $table->string("kualifikasi");
            $table->string("klasifikasi");
            $table->text("keterangan");
            $table->boolean("att");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_ijin_usaha");
    }
}
