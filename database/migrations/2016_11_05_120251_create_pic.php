<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pic', function(Blueprint $table) {
            $table->string('kode_penyedia', 10)->primary();
            $table->string('ktp', 30);
            $table->string('nama', 100);
            $table->string('jabatan', 100);
            $table->string('alamat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pic');
    }
}
