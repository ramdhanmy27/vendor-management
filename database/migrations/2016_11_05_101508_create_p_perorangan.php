<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePPerorangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_perorangan', function(Blueprint $table) {
            $table->string('kode_penyedia', 10)->primary();
            $table->string('kode_detail_jenis_penyedia', 5);
            $table->string('nama', 100);
            $table->string('no_identitas', 30);
            $table->string('npwp', 25);
            $table->string('tmp_lahir', 100);
            $table->date('tgl_lahir');
            $table->string('nama_ibu', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('p_perorangan');
    }
}
