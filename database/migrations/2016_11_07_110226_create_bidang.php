<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidang extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("bidang", function(Blueprint $table) {
			// $table->increments("id");
			$table->string("kode", 10)->primary();
			$table->string("nama", 100);
			$table->string("keterangan", 100);
			$table->char("kategori", 1);
			$table->boolean("status");
		});

		$this->seed();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("bidang");
	}

	public function seed() 
	{
		$ref = [
			["A.01.00", "EKSPLORASI, PRODUKSI DAN PENGOLAH LANJUTAN", "EXPLORATION, PRODUCTION AND PROCESSING", "A"],
			["A.02.00", "KONSTRUKSI, MEKANIKAL DAN ELEKTRIKAL", "CONSTRUCTION, MECHANICAL AND ELECTRICAL", "A"],
			["A.03.00", "BAHAN KIMIA DAN BAHAN PELEDAK", "CHEMICALS AND EXPLOSIVES", "A"],
			["A.04.00", "KANTOR, PERGUDANGAN, KESEHATAN DAN RUMAH TANGGA", "OFICE, WAREHOUSE, MEDICALS AND HOUEHOLD", "A"],
			["B.01.00", "PEKERJAAN SIPIL", "CIVIL WORKS", "B"],
			["B.02.00", "BIDANG MEKANIKAL/ELEKTRIKAL", "MECHANICAL AND ELECTRICAL", "B"],
			["B.03.00", "BIDANG RADIO, TELEKOMUNIKASI DAN INSTRUMENTASI", "RADIO, TELECOMMUNICATION AND INSTRUMENTATION", "B"],
			["B.04.00", "BIDANG LOGAM, KAYU DAN PLASTIK", "METALS, WOOD AND PLASTIC", "B"],
			["B.05.00", "BIDANG PERTANIAN", "AGRICULTURES", "B"],
			["B.06.00", "BIDANG PERTAMBANGAN MINYAK/GAS BUMI & PANAS BUMI", "OIL & GAS AND GEOTHERMAL", "B"],
			["D.01.00", "BIDANG PEKERJAAN UMUM", "BUILDING AND CONSTRUCTIONS", "D"],
			["D.02.00", "BIDANG TRANSPORTASI", "TRANSPORTATION", "D"],
			["D.03.00", "BIDANG PARIWISATA, POS DAN TELEKOMUNIKASI", "TOURISM, POST AND TELECOMMUNICATION", "D"],
			["D.04.00", "BIDANG PERTANIAN", "AGRICULTURE", "D"],
			["D.05.00", "BIDANG PERINDUSTRIAN", "INDUSTRY", "D"],
			["D.06.00", "BIDANG PEMBANGKITAN TENAGA", "POWER GENERATION", "D"],
			["D.07.00", "MENURUT LINGKUP LAYANAN PEKERJAAN", "GENERAL CONSULTANCY", "D"],
			["D.08.00", "BIDANG LAIN-LAIN", "OTHER CONSULTANCY", "D"],
			["C.00.00", "JASA LAINNYA", "C"],
		];

		$data = [];
		foreach ($ref as $item) {
			$data[] = [
				"kode" => current($item),
				"nama" => next($item),
				"keterangan" => next($item),
				"kategori" => next($item),
				"status" => true,
			];
		}

		DB::table("bidang")->insert($data);
	}
}
