<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePBadanHukum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_badan_hukum', function(Blueprint $table) {
            $table->string('kode_penyedia', 10)->primary();
            $table->string('kode_detail_jenis_penyedia', 5);
            $table->string('nama', 100);
            $table->string('npwp', 25);
            $table->string('no_reg', 20);
            $table->boolean('dpm');
            $table->string('akta', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('p_badan_hukum');
    }
}
