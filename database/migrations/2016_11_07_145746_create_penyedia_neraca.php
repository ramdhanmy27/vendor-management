<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaNeraca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_neraca", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->string("nama", 100);
            $table->date("tgl");
            $table->char("mata_uang", 1);
            $table->decimal("aktiva_tetap", 15, 2);
            $table->decimal("aktiva_lancar", 15, 2);
            $table->decimal("aktiva_lainnya", 15, 2);
            $table->decimal("aktiva", 15, 2);
            $table->decimal("kewajiban", 15, 2);
            $table->decimal("modal", 15, 2);
            $table->decimal("kewajiban_modal", 15, 2);
            $table->boolean("att");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_neraca");
    }
}
