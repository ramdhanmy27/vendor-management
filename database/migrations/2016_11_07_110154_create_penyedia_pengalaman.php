<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaPengalaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_pengalaman", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->string("pekerjaan", 100);
            $table->string("lokasi", 50);
            $table->string("instansi", 30);
            $table->string("alamat_instansi");
            $table->string("telp", 15);
            $table->string("no_kontrak", 30);
            $table->date("tgl_mulai");
            $table->date("tgl_akhir");
            $table->decimal("value", 15, 2);
            $table->string("currency", 5);
            $table->smallInteger("persentase");
            $table->date("tgl_bast");
            $table->boolean('att');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_pengalaman");
    }
}
