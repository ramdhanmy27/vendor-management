<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("bank", function(Blueprint $table) {
            $table->char("kode", 3);
            $table->string("nama", 50);
            $table->string("cabang", 50);
            $table->string("alamat");
            $table->text("keterangan");
            $table->boolean("status");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("bank");
    }
}
