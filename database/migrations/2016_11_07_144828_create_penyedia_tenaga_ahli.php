<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaTenagaAhli extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_tenaga_ahli", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->string("nama", 100);
            $table->string("tmp_lahir", 100);
            $table->date("tgl_lahir");
            $table->boolean("jk");
            $table->string("alamat");
            $table->boolean("wni");
            $table->string("email");
            $table->string("posisi", 50);
            $table->char("status", 1);
            $table->boolean("att");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_tenaga_ahli");
    }
}
