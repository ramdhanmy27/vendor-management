<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaPajak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_pajak", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->string("no_bukti", 20);
            $table->date("tgl");
            $table->boolean("jk");
            $table->boolean("wni");
            $table->integer("id_jenis_pajak");
            $table->date("berlaku_start");
            $table->date("berlaku_end");
            $table->boolean("att");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_pajak");
    }
}
