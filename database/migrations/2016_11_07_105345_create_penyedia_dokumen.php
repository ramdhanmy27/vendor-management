<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaDokumen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyedia_dokumen', function(Blueprint $table) {
            $table->increments('id');
            $table->string('kode_penyedia', 10);
            $table->string('nama', 100);
            $table->boolean('att');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penyedia_dokumen');
    }
}
