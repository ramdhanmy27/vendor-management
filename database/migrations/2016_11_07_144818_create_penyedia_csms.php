<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaCsms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_csms", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->string("no_csms", 30);
            $table->integer("id_jenis_csms");
            $table->boolean('att');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_csms");
    }
}
