<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediaPemilik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("penyedia_pemilik", function(Blueprint $table) {
            $table->increments("id");
            $table->string("kode_penyedia", 10);
            $table->string("ktp", 20);
            $table->string("nama", 100);
            $table->date("tgl_lahir");
            $table->boolean("jk");
            $table->string("alamat");
            $table->boolean("wni");
            $table->string("telp", 15);
            $table->string("email");
            $table->smallInteger("saham");
            $table->boolean("att");
            $table->boolean("foto");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("penyedia_pemilik");
    }
}
