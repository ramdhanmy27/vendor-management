{{-- 
	Datatables AJAX
	@param  String $url
	@param  Array[field => [values]] $attr 
--}}

<table class="table table-bordered" datatable="{!! url($url) !!}">
	<thead>
		<tr>
		@foreach ($field as $key => $attr)
			<?php
				$attributes = "";

				if (is_array($attr)) {
					foreach ($attr as $f => $v) {
						$attributes .= "$f='$v' ";
					}
				}
				// label only
				else $attr = ["label" => $attr];
			?>

			<th dt-field="{{ $key }}" {!! $attributes !!}>
				{{ $attr["label"] or ucwords(str_replace("_", " ", $key)) }}
			</th>
		@endforeach
		</tr>
	</thead>
</table>