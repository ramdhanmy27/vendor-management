<?php

use App\Models\Perorangan;
use App\Models\DetailJenisPenyedia;
use App\Models\Penyedia;

?>

@section("input", true)

{!! Form::model(Perorangan::class) !!}
	{!! Form::hidden("kode_jenis_penyedia", Penyedia::VENDOR_PO) !!}
	{!! Form::group("text", "nama", "Nama Lengkap") !!}
	{!! Form::group("text", "no_identitas", "No. KTP / Paspor") !!}
	{!! Form::group("text", "email", "Email", null, ["rules" => Penyedia::$rules["email"]]) !!}
	{!! Form::group("text", "npwp", "NPWP") !!}
	{!! Form::group("text", "tmp_lahir", "Tempat Lahir") !!}
	{!! Form::group("text", "tgl_lahir", "Tanggal Lahir") !!}
	{!! Form::group("text", "nama_ibu", "Nama Ibu kandung") !!}

	<div class="form-group">
		<button type="submit" class="col-md-offset-3 btn btn-primary">
			Daftar
		</button>
	</div>
{!! Form::close() !!}