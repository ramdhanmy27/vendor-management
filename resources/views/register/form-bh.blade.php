<?php

use App\Models\BadanHukum;
use App\Models\DetailJenisPenyedia;
use App\Models\Penyedia;

?>

@section("input", true)

{!! Form::model(BadanHukum::class) !!}
	{!! Form::hidden("kode_jenis_penyedia", Penyedia::VENDOR_BH) !!}
	{!! Form::group(
		"select", 
		"kode_detail_jenis_penyedia", 
		"Jenis Badan Hukum", 
		DetailJenisPenyedia::bh()->pluck("nama", "kode")
	) !!}
	{!! Form::group("text", "nama", "Nama Badan Hukum") !!}
	{!! Form::group("text", "email", "Email", null, ["rules" => Penyedia::$rules["email"]]) !!}
	{!! Form::group("text", "npwp", "NPWP") !!}
	{!! Form::group("text", "akta") !!}
	{!! Form::group("text", "no_reg", "No. Registrasi") !!}
	{!! Form::group("radios", "dpm", "DPM", BadanHukum::$opt["dpm"], ["1"]) !!}

	<div class="form-group">
		<button type="submit" class="col-md-offset-3 btn btn-primary">
			Daftar
		</button>
	</div>
{!! Form::close() !!}