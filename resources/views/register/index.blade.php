@extends('app')

@section("title", "Register")

@section('content')
    <div class="tabs">
        <ul class="nav nav-tabs">
            <li class="active"> <a href="#bh" data-toggle="tab">Badan Hukum</a> </li>
            <li> <a href="#bu" data-toggle="tab">Badan Usaha</a> </li>
            <li> <a href="#po" data-toggle="tab">Perorangan</a> </li>
        </ul>

        <div class="tab-content">
            <div id="bh" class="tab-pane active">
                @include("register.form-bh")
            </div>
            <div id="bu" class="tab-pane">
                @include("register.form-bu")
            </div>
            <div id="po" class="tab-pane">
                @include("register.form-po")
            </div>
        </div>
    </div>
@endsection