<?php

use App\Models\BadanUsaha;
use App\Models\DetailJenisPenyedia;
use App\Models\Penyedia;

?>

@section("input", true)

{!! Form::model(BadanUsaha::class) !!}
	{!! Form::hidden("kode_jenis_penyedia", Penyedia::VENDOR_BU) !!}
	{!! Form::group(
		"select", 
		"kode_detail_jenis_penyedia", 
		"Jenis Badan Usaha", 
		DetailJenisPenyedia::bu()->pluck("nama", "kode")
	) !!}
	{!! Form::group("text", "nama", "Nama Perusahaan") !!}
	{!! Form::group("text", "email", "Email", null, ["rules" => Penyedia::$rules["email"]]) !!}
	{!! Form::group("text", "npwp", "NPWP") !!}
	{!! Form::group("text", "ppkp", "PPKP") !!}

	<div class="form-group">
		<button type="submit" class="col-md-offset-3 btn btn-primary">
			Daftar
		</button>
	</div>
{!! Form::close() !!}