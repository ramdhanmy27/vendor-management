@extends("app")

@section("title", $crud->model_title)

@section("content")
<div class="panel panel-featured panel-featured-primary">
	<div class="panel-heading">
		<h3 class="panel-title">@yield("title")</h3>
	</div>
	
	<div class="panel-body">
		{!! $grid !!}
	</div>
</div>
@endsection