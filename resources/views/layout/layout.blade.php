<!DOCTYPE HTML>
<html>
<head>
    <title> @yield('title', "ASSA Rent") </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset("img/favicon.ico") }}">

    <link rel="stylesheet" href="{{ asset("css/vendor.css") }}">
    @if ($__env->yieldContent("input"))
        <link rel="stylesheet" href="{{ asset("css/input.css") }}">
    @endif

    @stack("style")
    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
    @stack("app-style")
</head>

<body>
    <header id="header">
        <div class="container-fluid">
            {{-- Logo --}}
            <div class="header-logo">
                <a href="#" class="no-decoration">
                    <img src="{{ asset("img/logo.png") }}" height="25px"></img>
                    <b>ASSA Rent</b>
                </a>
            </div>

            {{-- Menu --}}
            <div class="pull-left">
                <div class="header-nav">
                    <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target="#menu-bar">
                        <i class="fa fa-bars"></i>
                    </button>

                    <?php
                        $header_nav = [
                            "vendor" => [
                                "label" => "Settings", 
                                "icon" => "fa fa-cog",
                                "on" => Auth::check(),
                            ],
                        ];
                    ?>

                    <div id="menu-bar" class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                        <nav>
                            <ul class="nav nav-pills">
                            @foreach ($header_nav as $url => $attr)
                                @if (isset($attr["on"]) && !$attr["on"])
                                    @continue
                                @endif

                                <li>
                                    <a href="{{ url($url) }}">
                                        <i class="{{ $attr["icon"] }}"></i>
                                        <span class="nowrap">{{ $attr["label"] }}</span>
                                    </a>
                                </li>
                            @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            {{-- Account --}}
            <div class="header-nav">
                <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                    <nav>
                        <ul class="nav nav-pills">
                            @if (Auth::check())
                                <li class="dropdown"> 
                                    <a href="{{ url("customer") }}">
                                        <i class="fa fa-user"></i> {{ Auth::user()->email }}
                                    </a> 

                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url("logout") }}">
                                                Logout
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @else
                                <li> <a href="{{ url("register") }}"><i class="fa fa-user"></i> Daftar</a> </li>
                                <li> <a href="{{ url("login") }}"><i class="fa fa-lock"></i> Login</a> </li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <!-- Body -->
    @yield("body")

    <div id="footer">
        <div class="container">
            <p class="text-muted">© Copyright 2016. All Rights Reserved.</p>
        </div>
    </div>

    <!-- Modals -->
    @include("ui.modal", ["id" => "modal-basic"])
    @include("ui.modal", ["id" => "modal-error"])
    @include("ui.modal", [
        "id" => "modal-confirm", 
        "footer" => 
            "<button type='button' data-dismiss='modal' class='btn btn-primary modal-accept'>Ya</button>
            <button type='button' data-dismiss='modal' class='btn btn-default modal-close'>Tidak</button>"
    ])

    <!-- loading image -->
    <div class="hide">
        <img src="{{ asset('img/loading.gif') }}" id="img-loading" />
    </div>

    <!-- Javascript -->
    <script src="{{ asset("js/vendor.js") }}"></script>
    @if ($__env->yieldContent("input"))
        <script src="{{ asset("js/input.js") }}"></script>
    @endif

    @stack("script")

    <script src="{{ asset("js/app.js") }}"> </script>
    <script>
        fn.url.base = '{{ url("/") }}/';

        $(document).ready(function() {
            @if (Notif::exists())
                var notification = {!! json_encode(Notif::pull()) !!};

                for (var type in notification) 
                    fn.notif(notification[type], type);
            @endif
        });
    </script>

    @stack("app-script")
</body>
</html>