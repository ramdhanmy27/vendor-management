@extends("front::app")

@section("title", "Page Not Found")

@section("content")
    <div class="page-not-found text-center">
	    <h2 class="text-danger">404 !</h2>
	    <p>We're sorry, but the page you were looking for doesn't exist.</p>
	</div>
@endsection
