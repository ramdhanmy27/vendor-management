<div class="header-nav">
    <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
        <i class="fa fa-bars"></i>
    </button>

    <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
        <nav>
            <ul class="nav nav-pills" id="mainNav">
			    {!! $menu !!}
            </ul>
        </nav>
    </div>
</div>