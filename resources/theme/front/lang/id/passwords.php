<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password minimal terdiri dari 6 karakter atau lebih dan harus sesuai dengan konfirmasi password.',
    'reset' => 'Password anda telah diganti!',
    'sent' => 'Email untuk melakukan penggantian password telah dikirim ke email anda!',
    'token' => 'Token untuk melakukan penggantian password tidak valid.',
    'user' => "Tidak dapat menemukan alamat email.",

];
