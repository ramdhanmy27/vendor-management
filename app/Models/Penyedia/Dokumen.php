<?php

namespace App\Models\Penyedia;

use App\Models\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Dokumen extends Model
{
    protected $table = "penyedia_dokumen";
    
    protected $fillable = ["nama", "_att", ];

	public static $rules = [
		"nama" => "required|max:100",
		"_att" => "file|mimes:pdf",
	];

	protected $attributes = [
		"att" => false,
	];

	// temporary uploaded file
    private $file;

    public function beforeSave()
    {
    	if ($this->kode_penyedia === null)
	    	$this->kode_penyedia = Auth::user()->kode_penyedia;

	    // will upload after save
    	if (isset($this->_att) && $this->_att->isValid()) {
	    	$this->att = true;
	    	$this->file = $this->_att;
    	}

	    unset($this->_att);
    }

    public function afterSave()
    {
    	if ($this->file != null)
	    	$this->uploadAtt($this->file);
    }

    protected function uploadAtt($file)
    {
    	$dir = "$this->kode_penyedia/dokumen";
    	Storage::makeDirectory($dir);

    	$file->move(disk_path()."/$dir", $this->id.".pdf");
    	$this->att = $file->isValid();
    }
}
