<?php

namespace App\Models;

use App\Models\Penyedia;
use Illuminate\Database\Eloquent\Model as BaseModel;

class DetailJenisPenyedia extends BaseModel
{
    protected $table = "detail_jenis_penyedia";

    protected $primaryKey = "kode";

    public $incrementing = false;

    public $timestamps = false;

    public function scopeBu($query)
    {
    	return $query->where("kode_jenis_penyedia", Penyedia::VENDOR_BU);
    }

    public function scopeBh($query)
    {
    	return $query->where("kode_jenis_penyedia", Penyedia::VENDOR_BH);
    }

    public function scopePo($query)
    {
    	return $query->where("kode_jenis_penyedia", Penyedia::VENDOR_PO);
    }
}
