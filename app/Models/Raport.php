<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Raport extends BaseModel
{
	// green
	const OK = 1;

	// yellow
	const WARNING = 2;

	// red
	const DANGER = 3;

	// black
	const BLACKLIST = 4;

	protected $table = "raport";

	protected $fillable = ["id", "nama", "color"];

    public $incrementing = false;

    public $timestamps = false;
}
