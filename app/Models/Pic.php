<?php

namespace App\Models;

class Pic extends Model
{
    protected $table = "pic";
    
    protected $fillable = [
    	"kode_penyedia",
		"ktp",
		"nama",
		"alamat",
		"jabatan",
	];

	public static $rules = [
		"kode_penyedia" => "required|max:10",
		"ktp" => "required|max:30",
		"nama" => "required|max:100",
		"jabatan" => "required|max:100",
		"alamat" => "required|max:255",
	];

    protected $primaryKey = "kode_penyedia";

    public $incrementing = false;

    public $timestamps = false;
}
