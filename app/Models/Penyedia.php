<?php

namespace App\Models;

use App\Models\Alamat;
use App\Models\Kontak;
use App\Models\Raport;
use Illuminate\Support\Facades\DB;

class Penyedia extends Model
{
	// status vendor
	const STATUS_REGISTER = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_APPROVED = 3;

	// jenis vendor
	const VENDOR_BH = "BH";
	const VENDOR_BU = "BU";
	const VENDOR_PO = "PO";

    protected $table = "penyedia";
    
    protected $fillable = [
    	"kode",
		"kode_jenis_penyedia",
		"email",
		"email_instansi",
		"kode_pos",
		"kode_provinsi",
		"kode_kota",
		"status",
		"raport",
		"approved_at",

		// custom
		"telp"
	];

	protected $attributes = [
		"raport" => Raport::OK,
		"status" => self::STATUS_REGISTER,
	];

	public static $rules = [
		// "kode" => "required|max:10",
		"kode_jenis_penyedia" => "required|max:5",
		"email" => "required|email|max:255",
		"approved_at" => "date",
	];

	public static $profile_rules = [
		"email_instansi" => "required|email|max:255",
		"kode_pos" => "required|numeric",
		// "kode_pos" => "required|numeric|digits_between:4,6",
		"kode_provinsi" => "required|max:2",
		"kode_kota" => "required|max:4",
	];

    protected $primaryKey = "kode";

    public $incrementing = false;

    public static function getIncCode($code)
    {
    	$num = DB::table("penyedia")
	    	->selectRaw("max(substr(kode, length(kode)-3))::int as num")
	    	->where("kode", "like", implode("/", [$code, date("y")])."%")
	    	->value("num");

    	return implode("/", [$code, date("y"), str_pad($num+1, 4, "0", STR_PAD_LEFT)]);
    }

    public function alamat()
    {
        return $this->hasMany(Alamat::class, "kode_penyedia", "kode");
    }

    public function kontak()
    {
        return $this->hasMany(Kontak::class, "kode_penyedia", "kode");
    }

    public function beforeSave()
    {
    	// incrementing code if not set
    	if ($this->kode == null)
	    	$this->kode = self::getIncCode($this->kode_jenis_penyedia);
    }
}
