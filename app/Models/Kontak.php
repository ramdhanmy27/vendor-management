<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Kontak extends Model
{
    protected $table = "kontak";

    protected $fillable = ["kode_penyedia", "jenis", "nomor"];

    public static $rules = [
        "kode_penyedia" => "required|max:10",
        "jenis" => "required|max:5",
        "nomor" => "digits_between:10,15",
    ];

    public static $number_rules = "numeric";

    public static $labelType = [
    	"telp" => "No Telepon",
        "fax" => "No Fax",
    	"hp" => "No HP",
    ];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * Batch insert Kontak
     * @param  string $kode_penyedia
     * @param  array $input
     */
    public static function insertAll($kode_penyedia, $input)
    {
        // abort insert
        if (!is_array($input) || count($input) == 0)
            return;

        // reset data back to zero
        self::where("kode_penyedia", $kode_penyedia)->delete();

        $data = [];

        foreach ($input as $jenis => $item) {
            foreach ($item as $nomor) {
                $row = [
                    "kode_penyedia" => $kode_penyedia,
                    "jenis" => $jenis,
                    "nomor" => $nomor,
                ];

                if (empty($nomor))
                    continue;
            
                self::validateDataOrFail($row);
                $data[] = $row;
            }
        }

        // batch insert
        DB::table("kontak")->insert($data);
    }
}
