<?php

namespace App\Models;

class BadanHukum extends Model
{
    protected $table = "p_badan_hukum";
    
    protected $fillable = [
    	"kode_penyedia",
		"kode_detail_jenis_penyedia",
		"nama",
		"npwp",
		"no_reg",
		"dpm",
		"akta",
	];

	public static $rules = [
		"kode_penyedia" => "required|max:10",
		"kode_detail_jenis_penyedia" => "required|max:5",
		"nama" => "required|max:100",
		"npwp" => "required|max:25",
		"no_reg" => "required|max:20",
		"dpm" => "required",
		"akta" => "required|max:20",
	];

	public static $opt = [
		"dpm" => ["1" => "Ya", "0" => "Tidak"],
		"golongan" => [
			"UK" => "Usaha Kecil",
			"UM" => "Usaha Menengah",
			"UB" => "Usaha Besar",
		],
	];

    protected $primaryKey = "kode_penyedia";

    public $incrementing = false;

    public $timestamps = false;
}
