<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class JenisPenyedia extends BaseModel
{
    protected $table = "jenis_penyedia";

    protected $primaryKey = "kode";

    public $incrementing = false;

    public $timestamps = false;
}
