<?php

namespace App\Models;

class Kota extends Model
{
    protected $table = "kota";
    
    protected $primaryKey = "kode_kota";

    public $incrementing = false;

    public $timestamps = false;
}
