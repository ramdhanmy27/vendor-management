<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Alamat extends Model
{
    protected $table = "alamat";

    protected $fillable = ["kode_penyedia", "alamat"];

    public static $rules = [
        "kode_penyedia" => "required|max:10",
    	"alamat" => "max:255",
    ];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * Batch insert Alamat
     * @param  string $kode_penyedia
     * @param  array $input
     */
    public static function insertAll($kode_penyedia, $input)
    {
    	// abort insert
    	if (!is_array($input) || count($input) == 0)
    		return;

    	// reset data back to zero
    	self::where("kode_penyedia", $kode_penyedia)->delete();

    	$data = [];

    	foreach ($input as $item) {
    		$row = [
    			"kode_penyedia" => $kode_penyedia,
    			"alamat" => $item,
    		];

            if (!is_numeric($item) && empty($item))
                continue;

    		self::validateDataOrFail($row);
    		$data[] = $row;
    	}

    	// batch insert
    	DB::table("alamat")->insert($data);
    }
}
