<?php

namespace App\Models;

class Provinsi extends Model
{
    protected $table = "provinsi";
    
    protected $primaryKey = "kode_provinsi";

    public $incrementing = false;

    public $timestamps = false;
}
