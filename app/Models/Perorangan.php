<?php

namespace App\Models;

class Perorangan extends Model
{
    protected $table = "p_perorangan";
    
    protected $fillable = [
    	"kode_penyedia",
		"kode_detail_jenis_penyedia",
		"nama",
		"no_identitas",
		"npwp",
		"tmp_lahir",
		"tgl_lahir",
		"nama_ibu",
	];

	public static $rules = [
		"kode_penyedia" => "required|max:10",
		"kode_detail_jenis_penyedia" => "required|max:5",
		"nama" => "required|max:100",
		"no_identitas" => "required|max:30",
		"npwp" => "required|max:25",
		"tmp_lahir" => "required|max:100",
		"tgl_lahir" => "required|date",
		"nama_ibu" => "required|max:100",
	];

    protected $primaryKey = "kode_penyedia";

    public $incrementing = false;

    public $timestamps = false;
}
