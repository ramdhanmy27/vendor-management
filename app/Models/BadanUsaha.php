<?php

namespace App\Models;

class BadanUsaha extends Model
{
    protected $table = "p_badan_usaha";
    
    protected $fillable = [
    	"kode_penyedia",
		"kode_detail_jenis_penyedia",
		"nama",
		"npwp",
		"ppkp",
		"golongan",
	];

	public static $rules = [
		"kode_penyedia" => "required|max:10",
		"kode_detail_jenis_penyedia" => "required|max:5",
		"nama" => "required|max:100",
		"npwp" => "required|max:25",
		"ppkp" => "required|max:30",
		"golongan" => "required|max:100",
		// "golongan" => "required|max:100",
	];

	public static $golongan = [
		"UK" => "Usaha Kecil",
		"UM" => "Usaha Menengah",
		"UB" => "Usaha Besar",
	];

    protected $primaryKey = "kode_penyedia";

    public $incrementing = false;

    public $timestamps = false;
}
