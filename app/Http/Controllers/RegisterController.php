<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidatorException;
use App\Models\BadanHukum;
use App\Models\BadanUsaha;
use App\Models\Penyedia;
use App\Models\Perorangan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function getIndex()
    {
    	return view("register.index");
    }

    public function postIndex(Request $req)
    {
    	$data = $req->all();

    	try {
    		DB::beginTransaction();
    		
    		// create vendor 
	    	$vendor = Penyedia::createOrFail($req->only(["kode_jenis_penyedia", "email"]));
	    	$data["kode_penyedia"] = $vendor->kode;

	    	switch ($req->kode_jenis_penyedia) {
	    		case Penyedia::VENDOR_BH:
			    	BadanHukum::createOrFail($data);
	    			break;
	    			
	    		case Penyedia::VENDOR_BU:
                    $rules = BadanUsaha::$rules;
                    unset($rules["golongan"]);
                    
			    	BadanUsaha::createOrFail($data, $rules);
	    			break;

	    		case Penyedia::VENDOR_PO:
			    	Perorangan::createOrFail($data);
	    			break;
	    	}

	    	// create user login
            $password = "admin";
	    	// $password = User::getRandomPass();

	    	User::create([
	    		"email" => $data["email"],
	    		"kode_penyedia" => $data["kode_penyedia"],
	    		"password" => bcrypt($password),
	    	]);

    		DB::commit();
	    	return redirect("login");
    	}
    	catch (ValidatorException $e) {
    		DB::rollBack();

            return back()->withInput($data)
                ->withErrors($e->getValidator())
                ->withErrors($e->getMessages());
    	}
    	catch (\Exception $e) {
    		DB::rollBack();

    		return back()->withInput($data)
                ->withErrors($e->getMessage());
    	}
    }
}
