<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function() {
	Route::menu(["title" => "Settings", "url" => "vendor", "icon" => "fa fa-cog"]);
	
	Route::get("/", "LelangController@infoLelang");

	// Routes Auth;
	Route::get("login", "Auth\AuthController@showLoginForm");
	Route::post("login", "Auth\AuthController@login");
	Route::get("logout", "Auth\AuthController@logout");

	Route::controller("register", "RegisterController");
});