<?php

namespace App\Services\Widget\Menu\Factory;

use App\Models\Menu;
use App\Services\Widget\WidgetContract;
use DB as Query;
use Request;

class DB implements MenuFactoryContract, WidgetContract {
	
	protected $connection;

    private $current, $currentTmp, $lenTmp, $collection;

    public $path;

	public function __construct() {
		$this->connection = config("menu.drivers.db");
	}

    public function getCurrent() {
        if ($this->collection === null)
            $this->getCollection();

        if ($this->current === null && $this->currentTmp !== null) {
            $this->current = $this->currentTmp;
            $this->currentTmp = null;
        }

        return $this->current;
    }

	/**
	 * Delete menu instance
	 * also it's child if any
	 * 
	 * @return boolean
	 */
	public function delete() {
		// delete tree
		return Query::delete(
			"DELETE from menu where id in (
                with recursive r as (
                    select id, parent from menu where parent=?
                    union all
                    select m.id, m.parent from menu m
                    inner join r on r.id=m.parent
                )
                select id from r
            )", [$this->id]
        );
	}

	/**
	 * Check invalid order value
	 * update if necessary
	 */
	public function fixOrder() {
		$count = Query::select(
			"with recursive r as (
                select id, array[\"order\"] as order_path from menu where parent is null
                union all
                select m.id, order_path || m.\"order\" from menu m
                inner join r on r.id=m.parent
            )
            select count(*) from r 
            group by order_path having count(*)>1 
            order by order_path"
        );

		// don't need to reorder
        if (empty($count))
        	return;

        Query::update(
        	"UPDATE menu m set \"order\"=num-1 from ( 
                with recursive r as (
                    select row_number() over(order by id) as num, id from menu where parent is null
                        union all
                    select row_number() over(partition BY m.parent order by m.parent, m.id), m.id from menu m
                    inner join r on r.id=m.parent
                )
                select * from r
            ) as r
            where r.id=m.id"
        );
	}

    /**
     * Update list order
     * 
     * @param  int $order
     * @param  int $parent 
     * @return boolean
     */
	public function setOrder($id, $parent, $order) {
        $menu = Menu::findOrFail($id);
        $parent = empty($parent) || $parent==false ? null : $parent;

        if ($menu->parent==$parent && $menu->order==$order)
            return false;

        // old & new parent was differ
        if ($menu->parent != $parent) {
            # old parent group
            Menu::where([
                    ["order", ">", $menu->order],
                    ["parent", $menu->parent],
                ])
                ->update(["order" => Query::raw('"order" - 1')]);

            # new parent group
            Menu::where([
                    ["order", ">=", $order],
                    ["parent", $parent],
                ])
                ->update(["order" => Query::raw('"order" + 1')]);

            $menu->parent = $parent;
        }
        // sorting on same parent group
        else {
            // the new position order is less than the old one
            $isLessThan = $order < $menu->order;

            Menu::whereRaw("\"order\" between ? and ?", $isLessThan ? [$order, $menu->order-1] : [$menu->order+1, $order])
                ->where("parent", $menu->parent)
                ->update(["order" => Query::raw($isLessThan ? '"order" + 1' : '"order" - 1')]);
        }
                
        $menu->order = $order;

        return $menu->save();
	}

	/**
	 * Render tree branches
	 * 
	 * @param  array $tree
	 * @return String
	 */
	protected function renderTree(array $tree) {
		$html = null;

		foreach ($tree as $menu) {
            if (!$menu->enable)
                continue;

			if (isset($menu->child))
				$menu->childView = $this->renderTree($menu->child);

            $current = $this->getCurrent();

			$html .= view("layout.menu-tree", [
				"active" => $current === null ? false : $menu->id==$current->id,
				"menu" => $menu,
			])->render();
		}

		return $html;
	}

	/**
	 * Get current menu path
	 * 
	 * @param  integer $id
	 * @return array
	 */
	public function getPath($id = null) {
        if ($this->path === null)
            $this->path = array_reverse($this->findPath(
                $this->getCollection(), 
                $id===null ? $this->getCurrent() : $this->collection[$id])
            );

        return $this->path;
	}

    protected function findPath($collection, $current) {
        if ($current === null)
            return [];

        $path = [[
            "title" => $current->title,
            "url" => $current->url,
        ]];

        foreach ($collection as $item) {
            if ($item->id === $current->parent) {
                $path = array_merge($path, $this->findPath($collection, $item));
                break;
            }
        }

        return $path;
    }

    public function getCollection() {
        if ($this->collection !== null)
            return $this->collection;

        $path = Request::path();

        $this->collection = collect(Query::select(
            "with recursive r as (
                select *, array[id] as path, array[\"order\"] as order_path, 0 as level from menu where parent is null
                union all
                select m.*, path || m.id, order_path || m.\"order\", level+1 from menu m
                inner join r on r.id=m.parent
            )
            select * from r order by order_path"
        ))
        ->keyBy(function($item) use ($path) {
            // set current active menu
            if ($this->current === null && !empty($item->url)) {
                // current url is excatly same
                if ($item->url == $path) {
                    $this->current = $item;
                    $this->currentTmp = null;
                }
                // some url part is same
                else if (strpos($path, $item->url) !== false) {
                    // update currentTmp if menu->url is more detail
                    if ($this->currentTmp === null || $this->lenTmp < strlen($item->url)) {
                        $this->currentTmp = $item;
                        $this->lenTmp = strlen($item->url);
                    }
                }
            }

            return $item->id;
        });

        return $this->collection;
    }

	/** MenuFactoryContract Implementations */

	/**
	 * @inheritDocs
	 */
	public function getTree() {
		return $this->buildTree($this->getCollection());
	}

	/**
	 * Retrieving menu as tree of array
	 * 
	 * @param  array  $list
	 * @param  integer $parent
	 * @return array
	 */
	public function buildTree($list, $parent = null) {
		$branch = [];

        foreach ($list as $menu) {
            if ($menu->parent == $parent) {
                if ($children = $this->buildTree($list, $menu->id))
                    $menu->child = $children;

                $branch[] = $menu;
            }
        }

        return $branch;
	}

	/**
	 * Render menu as HTML
	 * 
	 * @return View
	 */
	public function render() {
		return view("layout.menu", [
			"menu" => $this->renderTree($this->getTree()),
		]);
	}

	/**
	 * Render Menu path as breadcrumb
	 * 
	 * @return View
	 */
	public function renderBreadcrumb() {
		return view("layout.breadcrumb", [
			"path" => $this->getPath(),
		])->render();
	}

    public function breadcrumbExists() {
        return count($this->getPath()) > 0;
    }
}