<?php

namespace App\Services;

class ServiceResponse {

	const ERROR = "error";

	// status code : 200
	const SUCCESS = "success";
	const WARNING = "warning";
	const INFO = "info";

	public $format, $status, $message, $data;
	public $statusCode = 500;

	private $response = [];
	protected $ref_format = ["json", "xml"];

	/**
	 * Create new response
	 * 
	 * @param string|integer $status
	 * @param string|array $message
	 * @param string $format
	 */
	public function __construct($status, $message = null, $format = "json") {
		// status code [integer]
		if (is_numeric($status)) {
			$this->statusCode = $status;
			$this->status = $status==200 ? self::SUCCESS : self::ERROR;
		}
		// array
		// default -> success : 200
		else if (is_array($status)) {
			$this->status = isset($status["status"]) ? $status["status"] : "success";
			$this->statusCode = isset($status["statusCode"]) ? $status["statusCode"] : 200;
		}
		// status name [string]
		else {
			$this->status = $status;
			$this->statusCode = $status==self::ERROR ? 500 : 200;
		}

		$this->message = $message;
		$this->format = $format;

		// response data
		$this->response = [
			"status" => $this->status,
			"statusCode" => $this->statusCode,
			"message" => $this->message,
		];
	}

	/**
	 * Build fast response
	 * 
	 * @param string|integer $status
	 * @param string|array $message
	 * @param string $format
	 */
	public static function make($status, $message = null, $format = "json") {
		return (new self($status, $message, $format))->build();
	}

	/**
	 * Return filtered response data
	 * 
	 * @return array
	 */
	public function getResponseData() {
		return array_filter($this->response);
	}

	/**
	 * return data response
	 * 
	 * @return JSON
	 */
	public function build() {
		if (!in_array($this->format, $this->ref_format))
			throw new ServiceException("Format $this->format is not supported.");

		switch ($this->format) {
			case 'json':
				return response()->json($this->getResponseData());
			
			case "xml":
				return response(xml_encode($this->getResponseData()), $this->statusCode);
		}
	}
}