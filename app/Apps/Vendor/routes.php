<?php

Route::group(["middleware" => ["web", "auth", "config:menu=router"]], function() {
	Route::menu(["title" => "Profile", "url" => "vendor", "icon" => "fa fa-user"]);

	/** Attachment Badan Usaha */
	Route::group(["namespace" => "Penyedia"], function() {
		Route::controller("dokumen", "DokumenController")
			->menu("Dokumen", "/", "fa fa-file-pdf-o");
	});

	Route::controller("/", "ProfileController");
});