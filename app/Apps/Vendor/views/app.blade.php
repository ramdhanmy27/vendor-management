@extends('layout.layout')

@section('body')
    <div class="content-wrapper">
        <div class="container">
            <div class="mt-xlg">
                <!-- Flash Messages -->
                @if (Flash::exists())
                    @foreach (Flash::pull() as $state => $msg)
                        <div class='alert alert-{{ $state }}'>
                            <button type="button" class="close" data-dismiss="alert">
                                <span>&times;</span>
                            </button>

                            {!! "<li>".implode("</li><li>", $msg)."</li>" !!}
                        </div>
                    @endforeach
                @endif

                {{-- Errors --}}
                @if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button>
                        
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-3">
                        {!! Menu::render() !!}
                    </div>

                    <div class="col-md-9">
                        <div class="heading heading-secondary heading-border heading-bottom-border">
                            <h1>
                                @yield("title")
                                @yield("content-header")
                            </h1>
                        </div>

                        @yield("content")
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection