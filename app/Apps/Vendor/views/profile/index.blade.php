<?php use App\Models\Penyedia; ?>

@extends("app::app")

@section("title", "Profile")

@section("content")
	@if (Penyedia::VENDOR_BH)
	    @include("profile.form-bh", ["model" => $model])
	@elseif (Penyedia::VENDOR_BU)
	    @include("profile.form-bu", ["model" => $model])
	@elseif (Penyedia::VENDOR_PO)
	    @include("profile.form-po", ["model" => $model])
	@endif
@endsection
