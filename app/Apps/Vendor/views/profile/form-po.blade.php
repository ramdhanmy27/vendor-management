<?php

use App\Models\Kontak;
use App\Models\Kota;
use App\Models\Penyedia;
use App\Models\Perorangan;
use App\Models\Pic;
use App\Models\Provinsi;

$kontak = $model->kontak->groupBy("jenis");
$alamat = $model->alamat;

?>

@section("input", true)

{!! Form::model($model, ["enctype" => "multipart/form-data"]) !!}
    {!! Form::rules(Pic::$rules) !!}
    {!! Form::rules(Penyedia::$profile_rules) !!}
    {!! Form::rules(Perorangan::$rules) !!}

    {!! Form::group("text", "nama") !!}
    {!! Form::group("text", "ktp", "No KTP") !!}
    {!! Form::group("text", "tmp_lahir", "Tempat Lahir") !!}
    {!! Form::group("date", "tgl_lahir", "Tanggal Lahir") !!}
    {!! Form::group("text", "nama_ibu", "Nama Ibu Kandung") !!}
    {!! Form::group("text", "email") !!}

    @for ($i = 0; $i < 2; $i++)
	    {!! Form::group(
	    	"textarea", "alamat[]", "Alamat ".($i+1), 
	    	isset($alamat[$i]) ? $alamat[$i] : "", 
	    	["rules" => $i==0 ? "required" : ""]
	    ) !!}
    @endfor

    {!! Form::group("text", "kode_pos") !!}
    {!! Form::group("select", "kode_provinsi", "Provinsi", Provinsi::pluck("nama", "kode")) !!}
    {!! Form::group("select", "kode_kota", "Kota", Kota::pluck("nama", "kode")) !!}

    @foreach (Kontak::$labelType as $key => $label)
	    @for ($i = 0; $i < 2; $i++)
		    {!! Form::group(
		    	"text", "telp[$key]", "No $label ".($i+1), 
		    	@$kontak[$key][$i], 
		    	["rules" => $i==0 ? "required" : ""]
	    	) !!}
	    @endfor
    @endforeach

    {!! Form::group("text", "npwp", "NPWP") !!}

    <div class="form-group">
        <button type="submit" class="col-md-offset-3 btn btn-primary"> Simpan </button>
    </div>
{!! Form::close() !!}