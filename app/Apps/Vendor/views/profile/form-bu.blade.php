<?php

use App\Models\BadanUsaha;
use App\Models\Kontak;
use App\Models\Kota;
use App\Models\Penyedia;
use App\Models\Pic;
use App\Models\Provinsi;

$kontak = $model->kontak->groupBy("jenis");
$alamat = $model->alamat;

?>

@section("input", true)

{!! Form::model($model, ["enctype" => "multipart/form-data"]) !!}
    {!! Form::rules(Pic::$rules) !!}
    {!! Form::rules(Penyedia::$profile_rules) !!}
    {!! Form::rules(BadanUsaha::$rules) !!}

    {!! Form::group("text", "nama") !!}
    {!! Form::group("text", "npwp", "NPWP") !!}
    {!! Form::group("text", "pkp", "PKP") !!}
    {!! Form::group("text", "email") !!}
    {!! Form::group("text", "email_instansi", "Email Perusahaan") !!}
    {!! Form::group("select", "golongan", "Golongan Usaha", BadanUsaha::$golongan) !!}

    @for ($i = 0; $i < 3; $i++)
	    {!! Form::group(
	    	"textarea", "alamat[]", 
	    	$i==0 ? "Alamat Perusahaan" : "Alamat Cabang $i", 
	    	isset($alamat[$i]) ? $alamat[$i] : "", 
	    	["rules" => $i==0 ? "required" : ""]
	    ) !!}
    @endfor

    {!! Form::group("text", "kode_pos") !!}
    {!! Form::group("select", "kode_provinsi", "Provinsi", Provinsi::pluck("nama", "kode")) !!}
    {!! Form::group("select", "kode_kota", "Kota", Kota::pluck("nama", "kode")) !!}

    @foreach (Kontak::$labelType as $key => $label)
	    @for ($i = 0; $i < 2; $i++)
		    {!! Form::group(
		    	"text", "telp[$key]", "$label ".($i+1), 
		    	@$kontak[$key][$i], 
		    	["rules" => $i==0 ? "required" : ""]
	    	) !!}
	    @endfor
    @endforeach

    <div class="form-group">
        <button type="submit" class="col-md-offset-3 btn btn-primary"> Simpan </button>
    </div>
{!! Form::close() !!}