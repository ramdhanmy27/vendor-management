<?php

use App\Models\Alamat;
use App\Models\BadanHukum;
use App\Models\Kontak;
use App\Models\Kota;
use App\Models\Penyedia;
use App\Models\Pic;
use App\Models\Provinsi;

$alamat = $model->alamat()->get();
$kontak = $model->kontak->groupBy("jenis");

?>

@section("input", true)

{!! Form::model($model, ["enctype" => "multipart/form-data"]) !!}
    {!! Form::rules(Pic::$rules) !!}
    {!! Form::rules(Penyedia::$profile_rules) !!}
    {!! Form::rules(BadanHukum::$rules) !!}

    {!! Form::group("text", "nama", "Nama Badan Hukum") !!}
    {!! Form::group("text", "no_reg", "No Registrasi") !!}
    {!! Form::group("text", "email") !!}
    {!! Form::group("text", "email_instansi", "Email Badan Hukum") !!}
    {!! Form::group("radios", "dpm", "Daftar Penyedia Mampu", BadanHukum::$opt["dpm"]) !!}
    {{-- {!! Form::group("select", "golongan", "Golongan Usaha", BadanHukum::$opt["golongan"]) !!} --}}
    {!! Form::group("text", "akta", "No Akta") !!}

    @for ($i = 0; $i < 3; $i++)
        {!! Form::group(
            "textarea", "alamat_p[$i]", 
            $i==0 ? "Alamat Perusahaan" : "Alamat Cabang $i", 
            isset($alamat[$i]) ? $alamat[$i]->alamat : "", 
            ["rules" => ($i==0 ? "required|" : "").Alamat::$rules["alamat"]]
        ) !!}
    @endfor

    {!! Form::group("text", "kode_pos") !!}
    {!! Form::group("select", "kode_provinsi", "Provinsi", Provinsi::pluck("nama", "kode")) !!}
    {!! Form::group("select", "kode_kota", "Kota", Kota::pluck("nama", "kode")) !!}

    @foreach (Kontak::$labelType as $key => $label)
        @for ($i = 0; $i < 2; $i++)
            {!! Form::group(
                "text", "kontak_p[$key][$i]", "$label ".($i+1), 
                @$kontak[$key][$i]->nomor, 
                ["rules" => ($i==0 ? "required|" : "").Kontak::$number_rules]
            ) !!}
        @endfor
    @endforeach

    {!! Form::group("text", "npwp", "NPWP") !!}
    {!! Form::group("text", "nama_pic", "Nama PIC", null, ["rules" => Pic::$rules["nama"]]) !!}
    {!! Form::group("text", "jabatan", "Jabatan PIC") !!}
    {!! Form::group("text", "ktp", "No KTP PIC") !!}
    {!! Form::group("textarea", "alamat", "Alamat PIC") !!}

    <div class="form-group">
        <button type="submit" class="col-md-offset-3 btn btn-primary"> Simpan </button>
    </div>
{!! Form::close() !!}