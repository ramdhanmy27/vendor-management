@extends("app::app")

@section("title", "Dokumen")

@section("content-header")
	<a href="{{ url("vendor/dokumen") }}" class="btn btn-primary mt-sm ml-sm pull-right">
        <i class="fa fa-reply"></i> Kembali
    </a>
@endsection

@section("content")
    @include("penyedia.dokumen.form", [
    	"model" => App\Models\Penyedia\Dokumen::class,
    ])
@endsection
