@section("input", true)

{!! Form::model($model, ["enctype" => "multipart/form-data"]) !!}
    {!! Form::group("text", "nama") !!}
    {!! Form::group("file", "_att", "Attachment", ["rules" => is_string($model) ? "required" : ""]) !!}

    <div class="form-group">
        <button type="submit" class="col-md-offset-3 btn btn-primary"> Simpan </button>
    </div>
{!! Form::close() !!}