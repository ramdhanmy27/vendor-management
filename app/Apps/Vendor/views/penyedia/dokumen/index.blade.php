@extends('app::app')

@section("title", "Dokumen")

@section('content-header')
	<a href="{{ url("vendor/dokumen/add") }}" class="btn btn-primary mt-sm pull-right">
        <i class="fa fa-plus"></i> Tambah
    </a>
@endsection

@section('content')
    <table class="table table-bordered" datatable="{!! url("vendor/dokumen/data") !!}">
        <thead>
            <tr>
                <th dt-field="kode_penyedia"> Kode Penyedia </th>
                <th dt-field="nama"> Nama </th>
                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("dokumen/view/[[id]]") }}" class="btn btn-sm btn-info">
	                <i class="fa fa-eye"></i>
	            </a>
	            <a href="{{ url("dokumen/edit/[[id]]") }}" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("dokumen/delete/[[id]]") }}" class="btn btn-sm btn-danger" 
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
