@extends("app::app")

@section("title", "Dokumen")

@section("content-header")
    <a href="{{ url("vendor/dokumen") }}" class="btn btn-primary mt-sm pull-right">
        <i class="fa fa-reply"></i> Kembali
    </a>
    {{-- Update & Delete Buttons
    <a href="{{ url("vendor/dokumen") }}" class="btn btn-tertiary mt-sm ml-sm pull-right" method="post" 
        confirm="Anda yakin ingin menghapus ?">
        <i class="fa fa-trash"></i> Delete
    </a>
    <a href="{{ url("vendor/dokumen") }}" class="btn btn-secondary mt-sm ml-sm pull-right">
        <i class="fa fa-edit"></i> Update
    </a> --}}
@endsection

@section("content")
    <div class="table-responsive">
        <table class="table table-border table-striped">
            <tr>
                <th class="text-right">Nama</th>
                <td>{{ $model->nama }}</td>
            </tr>
            <tr>
                <th class="text-right">Dokumen</th>
                <td>blah</td>
            </tr>
        </table>
    </div>
@endsection