<?php

namespace App\Apps\Vendor\Controllers\Penyedia;

use App\Models\Penyedia\Dokumen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;

class DokumenController extends \App\Http\Controllers\Controller
{
    public function getIndex()
    {
    	return view([
    		"list" => Dokumen::all(),
    	]);
    }

    public function anyData()
    {
        return Datatables::of(Dokumen::select("*"))->make(true);
    }

    public function getAdd()
    {
    	return view([]);
    }

    public function postAdd(Request $req)
    {
    	$model = Dokumen::createOrFail($req->all());
    	return redirect("vendor/dokumen/view/$model->id");
    }

    public function getEdit($id)
    {
    	$model = Dokumen::findOrFail($id);

    	return view([
    		"model" => $model,
    	]);
    }

    public function postEdit(Request $req, $id)
    {
    	Dokumen::findOrFail($id)->updateOrFail($req->all());
    	return redirect("vendor/dokumen/view/$id");
    }

    public function getView($id)
    {
    	$model = Dokumen::findOrFail($id);

    	return view([
    		"model" => $model,
    	]);
    }

    public function getDelete($id)
    {
    	Dokumen::findOrFail($id)->delete();
    	return redirect("vendor/dokumen");
    }
}
