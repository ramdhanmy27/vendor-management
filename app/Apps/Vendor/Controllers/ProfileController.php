<?php

namespace App\Apps\Vendor\Controllers;

use App\Http\Requests;
use App\Models\Alamat;
use App\Models\BadanHukum;
use App\Models\BadanUsaha;
use App\Models\Kontak;
use App\Models\Penyedia;
use App\Models\Perorangan;
use App\Models\Pic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends \App\Http\Controllers\Controller
{
    public function getIndex()
    {
        $query = Penyedia::where("kode", Auth::user()->kode_penyedia);

        switch ($query->firstOrFail()->kode_jenis_penyedia) {
            case Penyedia::VENDOR_BH:
                $query->join("p_badan_hukum as p", "p.kode_penyedia", "=", "penyedia.kode");
                $query->leftJoin("pic", "pic.kode_penyedia", "=", "penyedia.kode")
                    ->addSelect("pic.*", "pic.nama as nama_pic");
                break;
            
            case Penyedia::VENDOR_BU:
                $query->join("p_badan_usaha as p", "p.kode_penyedia", "=", "penyedia.kode");
                break;
            
            case Penyedia::VENDOR_PO:
                $query->join("p_perorangan as p", "p.kode_penyedia", "=", "penyedia.kode");
                break;
        }

        // dd($query->firstOrFail());
    	return view([
    		"model" => $query->addSelect("p.*", "penyedia.*")->firstOrFail(),
        ]);
    	// ])->withErrors("blah blahblah");
    }

    public function postIndex(Request $req) 
    {
        $input = $req->all();
        // DB::transaction(function() use ($req) {
        try {
            $penyedia = Auth::user()->penyedia;
            $penyedia->updateOrFail($input);

            // if (!$req->has("alamat_p.0"))
            //     throw new ValidatorException("Alamat Wajib Diisi");

            Alamat::insertAll($penyedia->kode, $req->alamat_p);
            Kontak::insertAll($penyedia->kode, $req->kontak_p);

            switch ($penyedia->kode_jenis_penyedia) {
                case Penyedia::VENDOR_BH:
                    BadanHukum::find($penyedia->kode)->updateOrFail($input);
                    $pic = Pic::find($penyedia->kode);

                    if ($pic == null)
                        Pic::createOrFail([
                            "kode_penyedia" => $penyedia->kode,
                            "nama" => $req->nama_pic,
                        ] + $input);
                    else
                        $pic->updateOrFail(["nama" => $req->nama_pic] + $input);
                    break;
                
                case Penyedia::VENDOR_BU:
                    BadanUsaha::find($penyedia->kode)->updateOrFail($input);
                    break;
                
                case Penyedia::VENDOR_PO:
                    Perorangan::find($penyedia->kode)->updateOrFail($input);
                    break;
            }
        }
        /*catch (ValidatorException $e) {
            DB::rollBack();

            // dd($e->getMessages(), $e->getValidator());

            return back()//->withInput($input)
                ->withErrors($e->getValidator())
                ->withErrors($e->getMessages());
        }*/
        catch (\Exception $e) {
            DB::rollBack();

            throw $e;
            // dd($e->getMessage());

            // return back()//->withInput($input)
            //     ->withErrors($e->getMessage());
        }
        // });

        return redirect("vendor");
    }
}
