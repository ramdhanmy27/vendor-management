<?php

namespace App\Apps\Admin\Controllers;

use App\Exceptions\ServiceException;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends \App\Http\Controllers\Controller {

	public function getIndex() {
		return view([]);
	}

	public function getAdd($parent = null) {
		$model = new Menu;
		$model->parent = $parent;

		return view("layout.crud.add", [
			"model" => $model,
		]);
	}

	public function getEdit($id) {
		return view("layout.crud.edit", [
			"model" => Menu::findOrFail($id),
		]);
	}

	public function postDelete($id) {
		$model = Menu::findOrFail($id);

		// move entire child to grand parent
		Menu::where("parent", $model->id)->update(["parent" => $model->parent]);

		// delete menu and fix order
        $model->delete();
        \Menu::driver("db")->fixOrder();

		return back();
	}

	public function postAdd(Request $req) {
        Menu::createOrFail($req->except("_token") + ["order" => 0]);
        return redirect("admin/menu");
	}

	public function postEdit(Request $req, $id) {
        Menu::findOrFail($id)->update($req->except("_token"));
        return redirect("admin/menu");
	}

	public function postSwitchEnable($id) {
        $model = Menu::findOrFail($id);
        $model->update(["enable" => !$model->enable]);

		return back();
	}

	public function postOrder(Request $req) {
		$success = \Menu::driver("db")->setOrder(
			$req->input("id"), 
			$req->input("parent"), 
			$req->input("order")
		);

		if (!$success)
			throw new ServiceException("Failed to update position");
	}
}
